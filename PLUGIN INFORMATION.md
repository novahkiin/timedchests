This file contains all of the plugin information. 
Currently, there are 4 types of chests based on your request. All messages are fully editable.

Commands:
- /timedchests
- Aliases: /timedchest, /timedc, /tchests, /tch

Permissions: 
- timedchests.command.main - Allows access to /timedchests 
- timedchests.chest.default - Allows usage of the default timed chest
- timedchests.chest.vip - Allows usage of the VIP timed chest
- timedchests.chest.vipplus - Allows usage of the VIPPlus timed chest
- timedchests.chest.mvip - Allows usage of the MVIP timed chest

Files:
- config.yml: Contains all of the changeable variables. All of the reward information will be in that file.
- times.yml: Contains all of the time storage for each player's timed chests. **Do not modify this file unless you know what you are doing**