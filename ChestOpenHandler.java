
# This file will be optimized in the future.

package me._novarider_.timedchests.events;

import me._novarider_.timedchests.core.TimedChests;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class ChestOpenHandler implements Listener {

    File a = new File("plugins" + File.separator + "TimedChests" + File.separator + "times.yml");
    YamlConfiguration tChests = YamlConfiguration.loadConfiguration(a);
    TimedChests plugin;

    public ChestOpenHandler(TimedChests instance) {
        plugin = instance;
    }

    public void loadTime() {
        try {
            tChests.load(a);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void saveTime() {
        try {
            tChests.save(a);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String colorText(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    @EventHandler
    public void onChestOpen(InventoryOpenEvent e) throws ParseException {
        Inventory inv = e.getInventory();
        Player p = (Player) e.getPlayer();
        if (inv != null && inv.getName().equals(colorText("&7Default Timed Chest"))) {
            e.setCancelled(true);
            if(!p.hasPermission("timedchests.chest.default")) {
                p.sendMessage(colorText(plugin.getConfig().getString("messages.cannot-use-this-chest").replace("%chest%", "Default")));
                return;
            }
            loadTime();
            if (tChests.getString(p.getUniqueId().toString() + ".default.nextUseDate") != null) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String df = tChests.getString(p.getUniqueId().toString() + ".default.nextUseDate");
                Date date1 = sdf.parse(df);
                Date date2 = sdf.parse(today);
                c.setTime(date2);
                if (date1.compareTo(date2) > 0) {
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.on-cooldown").replace("%date%", tChests.getString(p.getUniqueId().toString() + ".default.nextUseDate"))));
                    return;
                } else {
                    c.setTime(date2);
                    c.add(Calendar.DATE, 1);
                    String sf = sdf.format(c.getTime());
                    tChests.set(p.getUniqueId().toString() + ".default.nextUseDate", sf);
                    //tChests.set(p.getUniqueId().toString() + ".nextUseOpen", timed);
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                    saveTime();
                    Random r = new Random();
                    int y = plugin.getConfig().getStringList("commands.default").size();
                    int x = plugin.getConfig().getInt("rewards.amount.default");
                    int l = 0;
                    while (l < x) {
                        int z = r.nextInt(y);
                        String cmd = plugin.getConfig().getStringList("commands.default").get(z).replace("%player%", p.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                        l++;
                    }
                }
            }else{
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String sf = sdf.format(c.getTime());
                c.setTime(sdf.parse(today));
                tChests.set(p.getUniqueId().toString() + ".default.nextUseDate", sf);
                p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                saveTime();
                Random r = new Random();
                int y = plugin.getConfig().getStringList("commands.default").size();
                int x = plugin.getConfig().getInt("rewards.amount.default");
                int l = 0;
                while (l < x) {
                    int z = r.nextInt(y);
                    String cmd = plugin.getConfig().getStringList("commands.default").get(z).replace("%player%", p.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                    l++;
                }
            }
        }
        if (inv != null && inv.getName().equals(colorText("&eVIP Timed Chest"))) {
            e.setCancelled(true);
            if(!p.hasPermission("timedchests.chest.vip")) {
                p.sendMessage(colorText(plugin.getConfig().getString("messages.cannot-use-this-chest").replace("%chest%", "VIP")));
                return;
            }
            loadTime();
            if (tChests.getString(p.getUniqueId().toString() + ".vip.nextUseDate") != null) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String df = tChests.getString(p.getUniqueId().toString() + ".vip.nextUseDate");
                Date date1 = sdf.parse(df);
                Date date2 = sdf.parse(today);
                c.setTime(date2);
                if (date1.compareTo(date2) > 0) {
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.on-cooldown").replace("%date%", tChests.getString(p.getUniqueId().toString() + ".vip.nextUseDate"))));
                    return;
                } else {
                    c.setTime(date2);
                    c.add(Calendar.DATE, 1);
                    String sf = sdf.format(c.getTime());
                    tChests.set(p.getUniqueId().toString() + ".vip.nextUseDate", sf);
                    //tChests.set(p.getUniqueId().toString() + ".nextUseOpen", timed);
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                    saveTime();
                    Random r = new Random();
                    int y = plugin.getConfig().getStringList("commands.vip").size();
                    int x = plugin.getConfig().getInt("rewards.amount.vip");
                    int l = 0;
                    while (l < x) {
                        int z = r.nextInt(y);
                        String cmd = plugin.getConfig().getStringList("commands.vip").get(z).replace("%player%", p.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                        l++;
                    }
                }
            }else{
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String sf = sdf.format(c.getTime());
                c.setTime(sdf.parse(today));
                tChests.set(p.getUniqueId().toString() + ".vip.nextUseDate", sf);
                p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                saveTime();
                Random r = new Random();
                int y = plugin.getConfig().getStringList("commands.vip").size();
                int x = plugin.getConfig().getInt("rewards.amount.vip");
                int l = 0;
                while (l < x) {
                    int z = r.nextInt(y);
                    String cmd = plugin.getConfig().getStringList("commands.vip").get(z).replace("%player%", p.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                    l++;
                }
            }
        }
        if (inv != null && inv.getName().equals(colorText("&eVIPPlus Timed Chest"))) {
            e.setCancelled(true);
            if(!p.hasPermission("timedchests.chest.vipplus")) {
                p.sendMessage(colorText(plugin.getConfig().getString("messages.cannot-use-this-chest").replace("%chest%", "VIPPlus")));
                return;
            }
            loadTime();
            if (tChests.getString(p.getUniqueId().toString() + ".vipplus.nextUseDate") != null) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String df = tChests.getString(p.getUniqueId().toString() + ".vipplus.nextUseDate");
                Date date1 = sdf.parse(df);
                Date date2 = sdf.parse(today);
                c.setTime(date2);
                if (date1.compareTo(date2) > 0) {
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.on-cooldown").replace("%date%", tChests.getString(p.getUniqueId().toString() + ".vipplus.nextUseDate"))));
                    return;
                } else {
                    c.setTime(date2);
                    c.add(Calendar.DATE, 1);
                    String sf = sdf.format(c.getTime());
                    tChests.set(p.getUniqueId().toString() + ".vipplus.nextUseDate", sf);
                    //tChests.set(p.getUniqueId().toString() + ".nextUseOpen", timed);
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                    saveTime();
                    Random r = new Random();
                    int y = plugin.getConfig().getStringList("commands.vipplus").size();
                    int x = plugin.getConfig().getInt("rewards.amount.vipplus");
                    int l = 0;
                    while (l < x) {
                        int z = r.nextInt(y);
                        String cmd = plugin.getConfig().getStringList("commands.vipplus").get(z).replace("%player%", p.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                        l++;
                    }
                }
            }else{
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String sf = sdf.format(c.getTime());
                c.setTime(sdf.parse(today));
                tChests.set(p.getUniqueId().toString() + ".vipplus.nextUseDate", sf);
                p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                saveTime();
                Random r = new Random();
                int y = plugin.getConfig().getStringList("commands.vipplus").size();
                int x = plugin.getConfig().getInt("rewards.amount.vipplus");
                int l = 0;
                while (l < x) {
                    int z = r.nextInt(y);
                    String cmd = plugin.getConfig().getStringList("commands.vipplus").get(z).replace("%player%", p.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                    l++;
                }
            }
        }
        if (inv != null && inv.getName().equals(colorText("&bMVIP Timed Chest"))) {
            e.setCancelled(true);
            if(!p.hasPermission("timedchests.chest.mvip")) {
                p.sendMessage(colorText(plugin.getConfig().getString("messages.cannot-use-this-chest").replace("%chest%", "MVIP")));
                return;
            }
            loadTime();
            if (tChests.getString(p.getUniqueId().toString() + ".mvip.nextUseDate") != null) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String df = tChests.getString(p.getUniqueId().toString() + ".mvip.nextUseDate");
                Date date1 = sdf.parse(df);
                Date date2 = sdf.parse(today);
                c.setTime(date2);
                if (date1.compareTo(date2) > 0) {
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.on-cooldown").replace("%date%", tChests.getString(p.getUniqueId().toString() + ".mvip.nextUseDate"))));
                    return;
                } else {
                    c.setTime(date2);
                    c.add(Calendar.DATE, 1);
                    String sf = sdf.format(c.getTime());
                    tChests.set(p.getUniqueId().toString() + ".mvip.nextUseDate", sf);
                    //tChests.set(p.getUniqueId().toString() + ".nextUseOpen", timed);
                    p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                    saveTime();
                    Random r = new Random();
                    int y = plugin.getConfig().getStringList("commands.mvip").size();
                    int x = plugin.getConfig().getInt("rewards.amount.mvip");
                    int l = 0;
                    while (l < x) {
                        int z = r.nextInt(y);
                        String cmd = plugin.getConfig().getStringList("commands.mvip").get(z).replace("%player%", p.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                        l++;
                    }
                }
            }else{
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                String today = sdf.format(c.getTime());
                c.add(Calendar.DATE, 1);
                String sf = sdf.format(c.getTime());
                c.setTime(sdf.parse(today));
                tChests.set(p.getUniqueId().toString() + ".mvip.nextUseDate", sf);
                p.sendMessage(colorText(plugin.getConfig().getString("messages.daily-chest-opened")));
                saveTime();
                Random r = new Random();
                int y = plugin.getConfig().getStringList("commands.mvip").size();
                int x = plugin.getConfig().getInt("rewards.amount.mvip");
                int l = 0;
                while (l < x) {
                    int z = r.nextInt(y);
                    String cmd = plugin.getConfig().getStringList("commands.mvip").get(z).replace("%player%", p.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
                    l++;
                }
            }
        }
    }
}
