package me._novarider_.timedchests.core;

import me._novarider_.timedchests.commands.TimedChestCMD;
import me._novarider_.timedchests.events.ChestOpenHandler;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class TimedChests extends JavaPlugin {

    File a = new File("plugins" + File.separator + "TimedChests" + File.separator + "times.yml");

    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        if(!a.exists()) {
            try {
                a.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Player Time data file created!");
        }
        getCommand("timedchest").setExecutor(new TimedChestCMD(this));
        getServer().getPluginManager().registerEvents(new ChestOpenHandler(this), this);
    }
}
