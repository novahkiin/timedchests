package me._novarider_.timedchests.commands;

import me._novarider_.timedchests.core.TimedChests;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class TimedChestCMD implements CommandExecutor {

    TimedChests plugin;
    public TimedChestCMD(TimedChests instance) {
        plugin = instance;
    }

    public String colorText(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    @Override
    public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
        if(c.getName().equalsIgnoreCase("timedchests")) {
            if(!(s instanceof Player)) {
                s.sendMessage(colorText("&cOnly players can use this command!"));
                return true;
            }
            Player p = (Player) s;
            if(!s.hasPermission("timedchests.command.main")) {
                s.sendMessage(colorText((plugin.getConfig().getString("messages.no-permission"))));
                return true;
            }
            if(args.length < 1) {
                s.sendMessage(colorText("&3&lTimed&b&lChests &7- &b0.0.1-ALPHA"));
                TextComponent tc = new TextComponent();
                tc.setText(colorText("&3● &b/timedchests reload &7- (Hover)"));
                tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(colorText("&aReload the configuration files\n&7Click to run the command")).create()));
                tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/timedchests reload"));
                p.spigot().sendMessage(tc);
                TextComponent ts = new TextComponent();
                ts.setText(colorText("&3● &b/timedchests getChest <default,vip,vipplus,mvip> &7- (Hover)"));
                ts.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(colorText("&aGives you a timed chest to place\n&7Click to setup the command")).create()));
                ts.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/timedchests getChest "));
                p.spigot().sendMessage(ts);
                s.sendMessage(colorText("&e● The default cooldown is 24 hours. This will be changeable in the future."));
                return true;
            }
            if(args[0].equalsIgnoreCase("reload")) {
                plugin.reloadConfig();
                s.sendMessage(colorText(plugin.getConfig().getString("messages.config-reload")));
                return true;
            }
            else if(args[0].equalsIgnoreCase("getChest")) {
                if(args.length < 2) {
                    s.sendMessage(colorText(plugin.getConfig().getString("usages.getChest-command")));
                    return true;
                }else{
                    ItemStack chest;
                    List<String> lore = new ArrayList<>();
                    ItemMeta meta;
                    switch (args[1].toLowerCase()) {
                        case "default":
                            chest = new ItemStack(Material.CHEST);
                            meta = chest.getItemMeta();
                            meta.setDisplayName(colorText("&7Default Timed Chest"));
                            lore.add("");
                            lore.add(colorText("&aPlace me on the ground"));
                            lore.add(colorText("&ato create a &7Default Timed Chest"));
                            meta.setLore(lore);
                            chest.setItemMeta(meta);
                            p.getInventory().addItem(chest);
                            p.sendMessage(colorText(plugin.getConfig().getString("messages.timed-chest-received").replace("%chest%", "Default")));
                            break;
                        case "vip":
                            chest = new ItemStack(Material.CHEST);
                            meta = chest.getItemMeta();
                            meta.setDisplayName(colorText("&eVIP Timed Chest"));
                            lore.add("");
                            lore.add(colorText("&aPlace me on the ground"));
                            lore.add(colorText("&ato create a &eVIP Timed Chest"));
                            meta.setLore(lore);
                            chest.setItemMeta(meta);
                            p.getInventory().addItem(chest);
                            p.sendMessage(colorText(plugin.getConfig().getString("messages.timed-chest-received").replace("%chest%", "VIP")));
                            break;
                        case "vipplus":
                            chest = new ItemStack(Material.CHEST);
                            meta = chest.getItemMeta();
                            meta.setDisplayName(colorText("&eVIPPlus Timed Chest"));
                            lore.add("");
                            lore.add(colorText("&aPlace me on the ground"));
                            lore.add(colorText("&ato create a &eVIPPlus Timed Chest"));
                            meta.setLore(lore);
                            chest.setItemMeta(meta);
                            p.getInventory().addItem(chest);
                            p.sendMessage(colorText(plugin.getConfig().getString("messages.timed-chest-received").replace("%chest%", "VIPPLUS")));
                            break;
                        case "vip+":
                            chest = new ItemStack(Material.CHEST);
                            meta = chest.getItemMeta();
                            meta.setDisplayName(colorText("&eVIPPlus Timed Chest"));
                            lore.add("");
                            lore.add(colorText("&aPlace me on the ground"));
                            lore.add(colorText("&ato create a &eVIPPlus Timed Chest"));
                            meta.setLore(lore);
                            chest.setItemMeta(meta);
                            p.getInventory().addItem(chest);
                            p.sendMessage(colorText(plugin.getConfig().getString("messages.timed-chest-received").replace("%chest%", "VIPPLUS")));
                            break;
                        case "mvip":
                            chest = new ItemStack(Material.CHEST);
                            meta = chest.getItemMeta();
                            meta.setDisplayName(colorText("&bMVIP Timed Chest"));
                            lore.add("");
                            lore.add(colorText("&aPlace me on the ground"));
                            lore.add(colorText("&ato create a &bMVIP Timed Chest"));
                            meta.setLore(lore);
                            chest.setItemMeta(meta);
                            p.getInventory().addItem(chest);
                            p.sendMessage(colorText(plugin.getConfig().getString("messages.timed-chest-received").replace("%chest%", "MVIP")));
                            break;
                    }
                }
                return true;
            }else{
                s.sendMessage("");
                s.sendMessage(ChatColor.RED + "                              Invalid usage");
                s.sendMessage("");
                s.sendMessage(colorText("&3&lTimed&b&lChests &7- &b0.0.1-ALPHA"));
                s.sendMessage(colorText("&3● &b/timedchests reload &7- &bReload the configuration"));
                s.sendMessage(colorText("&3● &b/timedchests getChest <default,vip,vipplus,mvip> &7- &bGet a timed chest for a specific rank"));
                s.sendMessage(colorText("&e● The default cooldown is 24 hours. This will be changeable in the future."));
            }
            return true;
        }
        return false;
    }
}
